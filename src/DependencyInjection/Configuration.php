<?php

namespace Eazy\Bundle\MailjetBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('eazy_mailjet');
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
            ->scalarNode('api_key')->isRequired()->end()
            ->scalarNode('secret_key')->isRequired()->end()
            ->scalarNode('api_url')->defaultValue('api.mailjet.com')->end()
        ->end();

        return $treeBuilder;
    }
}