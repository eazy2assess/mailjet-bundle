<?php

namespace Eazy\Bundle\MailjetBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class EazyMailjetExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config =  $this->processConfiguration($configuration, $configs);

        $container->setParameter('eazy_mailjet.api_key', $config['api_key']);
        $container->setParameter('eazy_mailjet.secret_key', $config['secret_key']);
        $container->setParameter('eazy_mailjet.api_url', $config['api_url']);

        $fileLocator = new FileLocator(__DIR__ . '/../Resources/config');
        $yamlLoader  = new YamlFileLoader($container, $fileLocator);
        $yamlLoader->load('services.yaml');
    }
}