<?php

namespace Eazy\Bundle\MailjetBundle\Model\Contact;

interface CreateContactInterface
{
    public function getName(): ?string;

    public function getEmail(): string;

    public function isExcludedFromCampaigns(): bool;
}