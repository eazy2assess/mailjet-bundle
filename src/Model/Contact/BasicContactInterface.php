<?php

namespace Eazy\Bundle\MailjetBundle\Model\Contact;

interface BasicContactInterface
{
    public function getEmail(): string;

    public function getName(): ?string;

    public function getMailjetId(): int;
}