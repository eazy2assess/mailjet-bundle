<?php

namespace Eazy\Bundle\MailjetBundle\Model\Contact;

class Contact implements CreateContactInterface, BasicContactInterface
{
    /**
     * @var bool
     */
    private $isExcludedFromCampaigns;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var int
     */
    private $deliveryCount;

    /**
     * @var string
     */
    private $email;

    /**
     * @var \DateTime|null
     */
    private $exclusionFromCampaignsUpdatedAt;

    /**
     * @var int
     */
    private $id;

    /**
     * @var bool
     */
    private $isOptInPending;

    /**
     * @var bool
     */
    private $isSpamComplaining;

    /**
     * @var \DateTime|null
     */
    private $lastActivityAt;

    /**
     * @var \DateTime|null
     */
    private $lastUpdateAt;

    /**
     * @var \DateTime|null
     */
    private $unsubscribedAt;

    /**
     * @var \DateTime|null
     */
    private $unsubscribedBy;

    public function getMailjetId(): int
    {
        return $this->getId();
    }

    /**
     * @return bool
     */
    public function isExcludedFromCampaigns(): bool
    {
        return $this->isExcludedFromCampaigns;
    }

    /**
     * @param bool $isExcludedFromCampaigns
     */
    public function setIsExcludedFromCampaigns(bool $isExcludedFromCampaigns): void
    {
        $this->isExcludedFromCampaigns = $isExcludedFromCampaigns;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getDeliveryCount(): int
    {
        return $this->deliveryCount;
    }

    /**
     * @param int $deliveryCount
     */
    public function setDeliveryCount(int $deliveryCount): void
    {
        $this->deliveryCount = $deliveryCount;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return \DateTime|null
     */
    public function getExclusionFromCampaignsUpdatedAt(): ?\DateTime
    {
        return $this->exclusionFromCampaignsUpdatedAt;
    }

    /**
     * @param \DateTime|null $exclusionFromCampaignsUpdatedAt
     */
    public function setExclusionFromCampaignsUpdatedAt(?\DateTime $exclusionFromCampaignsUpdatedAt): void
    {
        $this->exclusionFromCampaignsUpdatedAt = $exclusionFromCampaignsUpdatedAt;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return bool
     */
    public function isOptInPending(): bool
    {
        return $this->isOptInPending;
    }

    /**
     * @param bool $isOptInPending
     */
    public function setIsOptInPending(bool $isOptInPending): void
    {
        $this->isOptInPending = $isOptInPending;
    }

    /**
     * @return bool
     */
    public function isSpamComplaining(): bool
    {
        return $this->isSpamComplaining;
    }

    /**
     * @param bool $isSpamComplaining
     */
    public function setIsSpamComplaining(bool $isSpamComplaining): void
    {
        $this->isSpamComplaining = $isSpamComplaining;
    }

    /**
     * @return \DateTime|null
     */
    public function getLastActivityAt(): ?\DateTime
    {
        return $this->lastActivityAt;
    }

    /**
     * @param \DateTime|null $lastActivityAt
     */
    public function setLastActivityAt(?\DateTime $lastActivityAt): void
    {
        $this->lastActivityAt = $lastActivityAt;
    }

    /**
     * @return \DateTime|null
     */
    public function getLastUpdateAt(): ?\DateTime
    {
        return $this->lastUpdateAt;
    }

    /**
     * @param \DateTime|null $lastUpdateAt
     */
    public function setLastUpdateAt(?\DateTime $lastUpdateAt): void
    {
        $this->lastUpdateAt = $lastUpdateAt;
    }

    /**
     * @return \DateTime|null
     */
    public function getUnsubscribedAt(): ?\DateTime
    {
        return $this->unsubscribedAt;
    }

    /**
     * @param \DateTime|null $unsubscribedAt
     */
    public function setUnsubscribedAt(?\DateTime $unsubscribedAt): void
    {
        $this->unsubscribedAt = $unsubscribedAt;
    }

    /**
     * @return \DateTime|null
     */
    public function getUnsubscribedBy(): ?\DateTime
    {
        return $this->unsubscribedBy;
    }

    /**
     * @param \DateTime|null $unsubscribedBy
     */
    public function setUnsubscribedBy(?\DateTime $unsubscribedBy): void
    {
        $this->unsubscribedBy = $unsubscribedBy;
    }
}