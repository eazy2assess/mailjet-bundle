<?php

namespace Eazy\Bundle\MailjetBundle\Model;

class Action
{
    const ADD_FORCE = 'addforce';
    const ADD_NO_FORCE = 'addnoforce';
    const REMOVE = 'remove';
    const UNSUB = 'unsub';
}