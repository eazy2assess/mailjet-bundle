<?php

namespace Eazy\Bundle\MailjetBundle\Model\ContactList;

interface CreateContactListInterface
{
    public function getName(): string;
}