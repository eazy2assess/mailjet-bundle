<?php

namespace Eazy\Bundle\MailjetBundle\Model\ContactList;

class CreateContactList implements CreateContactListInterface
{
    private $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }
}