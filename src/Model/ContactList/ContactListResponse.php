<?php

namespace Eazy\Bundle\MailjetBundle\Model\ContactList;

class ContactListResponse
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $address;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $subscriberCount;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime|null $createdAt
     */
    public function setCreatedAt(?\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getSubscriberCount(): int
    {
        return $this->subscriberCount;
    }

    /**
     * @param int $subscriberCount
     */
    public function setSubscriberCount(int $subscriberCount): void
    {
        $this->subscriberCount = $subscriberCount;
    }
}