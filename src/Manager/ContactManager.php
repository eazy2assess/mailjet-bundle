<?php

namespace Eazy\Bundle\MailjetBundle\Manager;

use Eazy\Bundle\MailjetBundle\Client\MailjetClient;
use Eazy\Bundle\MailjetBundle\Exception\MailjetClientException;
use Eazy\Bundle\MailjetBundle\Model\Contact\BasicContactInterface;
use Eazy\Bundle\MailjetBundle\Model\Contact\Contact;
use Eazy\Bundle\MailjetBundle\Model\Contact\CreateContactInterface;
use Mailjet\Resources;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class ContactManager implements ContactManagerInterface
{
    private $mailjet;

    private $serializer;

    public function __construct(MailjetClient $mailjet, SerializerInterface $serializer)
    {
        $this->mailjet = $mailjet;
        $this->serializer = $serializer;
    }

    public function createContact(CreateContactInterface $createContact): Contact
    {
        $apiResponse = $this->mailjet->post(Resources::$Contact, ['body' =>
            [
                'Name' => $createContact->getName(),
                'IsExcludedFromCampaigns' => $createContact->isExcludedFromCampaigns(),
                'Email' => $createContact->getEmail()
            ],
        ]);
        /** @var Contact $contact */
        $contact = $this->serializer->deserialize(
            \json_encode($apiResponse[0]),
            Contact::class,
            'json'
        );

        return $contact;
    }

    public function updateContact(BasicContactInterface $contact): void
    {
        $this->mailjet->put(Resources::$Contact, ['id' => $contact->getMailjetId(), 'body' =>
            [
                'Name' => $contact->getName(),
                'Email' => $contact->getEmail()
            ],
        ]);
    }

    public function deleteContact(BasicContactInterface $contact): void
    {
        $response = $this->mailjet->rawRequest(
            Request::METHOD_DELETE,
            sprintf('/v4/contacts/%s', $contact->getMailjetId())
        );

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            throw new MailjetClientException('Cannot delete contact', $response->getStatusCode());
        }
    }
}