<?php

namespace Eazy\Bundle\MailjetBundle\Manager;

use Eazy\Bundle\MailjetBundle\Client\MailjetClient;
use Eazy\Bundle\MailjetBundle\Model\Action;
use Eazy\Bundle\MailjetBundle\Model\Contact\BasicContactInterface;
use Eazy\Bundle\MailjetBundle\Model\ContactList\AddContactToListInterface;
use Eazy\Bundle\MailjetBundle\Model\ContactList\AddContactToListResponse;
use Eazy\Bundle\MailjetBundle\Model\ContactList\CreateContactList;
use Eazy\Bundle\MailjetBundle\Model\ContactList\CreateContactListInterface;
use Eazy\Bundle\MailjetBundle\Model\ContactList\ContactListResponse;
use Mailjet\Resources;
use Symfony\Component\Serializer\SerializerInterface;

class ContactListManager implements ContactListManagerInterface
{
    private $mailjet;

    private $serializer;

    public function __construct(MailjetClient $mailjet, SerializerInterface $serializer)
    {
        $this->mailjet = $mailjet;
        $this->serializer = $serializer;
    }

    public function createContactList(CreateContactListInterface $createContactList): ContactListResponse
    {
        $apiResponse = $this->mailjet->post(Resources::$Contactslist, ['body' => ['Name' => $createContactList->getName()]]);
        /** @var ContactListResponse $contactList */
        $contactList = $this->serializer->deserialize(
            \json_encode($apiResponse[0]),
            ContactListResponse::class,
            'json'
        );

        return $contactList;
    }

    public function addContactToList(string $listId, BasicContactInterface $contact, bool $force = false): void
    {
        $this->mailjet->post(Resources::$ContactManagecontactslists, [
            'id' => $contact->getMailjetId(),
            'body' => [
                'ContactsLists' => [
                    [
                        'Action' => $force ? Action::ADD_FORCE : Action::ADD_NO_FORCE,
                        'ListID' => $listId
                    ],
                ]
            ]
        ]);
    }

    public function deleteContactFromList(string $listId, BasicContactInterface $contact): void
    {
        $this->mailjet->post(Resources::$ContactManagecontactslists, [
            'id' => $contact->getMailjetId(),
            'body' => [
                'ContactsLists' => [
                    [
                        'Action' => Action::REMOVE,
                        'ListID' => $listId
                    ],
                ]
            ]
        ]);
    }

    public function deleteContactList(string $listId): void
    {
        $this->mailjet->delete(Resources::$Contactslist, ['id' => $listId]);
    }

    public function getContactList(string $listId): ContactListResponse
    {
        $response = $this->mailjet->get(Resources::$Contactslist, ['id' => $listId]);
        /** @var ContactListResponse $contactList */
        $contactList = $this->serializer->deserialize(
            \json_encode($response[0]),
            ContactListResponse::class,
            'json'
        );

        return $contactList;
    }

    public function getContactLists(int $offset = 0, int $limit = 20): array
    {
        $response = $this->mailjet->get(Resources::$Contactslist, ['Limit' => $limit, 'Offset' => $offset]);

        $contactLists = [];
        foreach ($response as $item) {
            $contactLists[] = $this->serializer->deserialize(
                \json_encode($item),
                ContactListResponse::class,
                'json'
            );
        }

        return $contactLists;
    }

    public function updateContactList(string $listId, CreateContactListInterface $createContactList): ContactListResponse
    {
        $apiResponse = $this->mailjet->put(Resources::$Contactslist, ['id' => $listId, 'body' => ['Name' => $createContactList->getName()]]);
        /** @var ContactListResponse $contactList */
        $contactList = $this->serializer->deserialize(
            \json_encode($apiResponse[0]),
            ContactListResponse::class,
            'json'
        );

        return $contactList;
    }
}