<?php

namespace Eazy\Bundle\MailjetBundle\Manager;

use Eazy\Bundle\MailjetBundle\Model\Contact\BasicContactInterface;
use Eazy\Bundle\MailjetBundle\Model\Contact\Contact;
use Eazy\Bundle\MailjetBundle\Model\Contact\CreateContactInterface;

interface ContactManagerInterface
{
    public function createContact(CreateContactInterface $createContact): Contact;

    public function deleteContact(BasicContactInterface $contact): void;

    public function updateContact(BasicContactInterface $contact): void;
}