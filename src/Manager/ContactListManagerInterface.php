<?php

namespace Eazy\Bundle\MailjetBundle\Manager;

use Eazy\Bundle\MailjetBundle\Model\Contact\BasicContactInterface;
use Eazy\Bundle\MailjetBundle\Model\ContactList\CreateContactList;
use Eazy\Bundle\MailjetBundle\Model\ContactList\CreateContactListInterface;
use Eazy\Bundle\MailjetBundle\Model\ContactList\ContactListResponse;

interface ContactListManagerInterface
{
    public function createContactList(CreateContactListInterface $createContactList): ContactListResponse;

    public function addContactToList(string $listId, BasicContactInterface $contact, bool $force = false): void;

    public function deleteContactFromList(string $listId, BasicContactInterface $contact): void;

    public function deleteContactList(string $listId): void;

    public function getContactList(string $listId): ContactListResponse;

    /**
     * @param int $offset
     * @param int $limit
     * @return ContactListResponse[]
     */
    public function getContactLists(int $offset = 0, int $limit = 20): array;

    public function updateContactList(string $listId, CreateContactListInterface $createContactList): ContactListResponse;
}