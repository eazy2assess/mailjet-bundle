<?php

namespace Eazy\Bundle\MailjetBundle\Exception;

class MailjetClientException extends \Exception
{
    public function __construct(string $message, int $code)
    {
        parent::__construct(
            \sprintf('Mailjet Api client error: %s code: %s', $message, $code)
        );
    }
}