<?php

namespace Eazy\Bundle\MailjetBundle\Client;

use Eazy\Bundle\MailjetBundle\Exception\MailjetClientException;
use Mailjet\Client;
use Mailjet\Response;
use Psr\Http\Message\ResponseInterface;

class MailjetClient
{
    private $client;

    private $apiKey;

    private $secretKey;

    private $httpClient;

    public function __construct(string $apiKey, string $secretKey, string $apiUrl)
    {
        $this->apiKey = $apiKey;
        $this->secretKey = $secretKey;
        $this->httpClient = new \GuzzleHttp\Client(['base_uri' => $apiUrl]);
        $this->client = new Client($apiKey, $secretKey, true, ['url' => $apiUrl]);
    }

    public function post($resource, array $arguments = [], array $options = []): array
    {
        $response = $this->client->post($resource, $arguments, $options);
        if (!$response->success()) {
            throw new MailjetClientException($response->getData()['ErrorMessage'] ?? '', $response->getStatus());
        }

        return $this->mapResponseData($response);
    }

    public function delete($resource, array $arguments = [], array $options = []): array
    {
        $response = $this->client->delete($resource, $arguments, $options);
        if (!$response->success()) {
            throw new MailjetClientException($response->getData()['ErrorMessage'] ?? '', $response->getStatus());
        }

        return $this->mapResponseData($response);
    }

    public function get($resource, array $arguments = [], array $options = []): array
    {
        $response = $this->client->get($resource, $arguments, $options);
        if (!$response->success()) {
            throw new MailjetClientException($response->getData()['ErrorMessage'] ?? '', $response->getStatus());
        }

        return $this->mapResponseData($response);
    }

    public function put($resource, array $arguments = [], array $options = []): array
    {
        $response = $this->client->put($resource, $arguments, $options);
        if (!$response->success()) {
            throw new MailjetClientException($response->getData()['ErrorMessage'] ?? '', $response->getStatus());
        }

        return $this->mapResponseData($response);
    }

    public function rawRequest(string $method, string $endpoint, array $options = []): ResponseInterface
    {
        $options = array_merge([
            'auth' => [$this->apiKey, $this->secretKey],
            'http_errors' => false,
        ], $options);

        return $this->httpClient->request($method, $endpoint, $options);
    }

    private function mapResponseData(Response $response): array
    {
        $mappedData = [];
        foreach ($response->getData() as $item) {
            $itemData = [];
            foreach ($item as $key => $property) {
                $itemData[\lcfirst($key)] = $property === '' ? null : $property;
            }
            $mappedData[] = $itemData;
        }

        return $mappedData;
    }
}